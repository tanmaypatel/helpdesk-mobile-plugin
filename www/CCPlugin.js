cordova.define("com.connectingcustomer.mobile.plugins.CCPlugin.CCPlugin", 
function(require, exports, module) 
{ 
	var exec = require('cordova/exec');

	exports.setFocus = function(success, error) 
	{
		exec(success, error, "CCPlugin", "setFocus", []);
	};

	exports.setSpeakerPhone = function(success, error, isEnabled) 
	{
		exec(success, error, "CCPlugin", "setSpeakerPhone", [isEnabled]);
	};

});