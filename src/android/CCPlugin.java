package com.connectingcustomer.mobile.plugins;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import android.media.AudioManager;
import android.content.Context;

public class CCPlugin extends CordovaPlugin {

  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

    if(action.equals("setFocus")) {
      Intent intent = new Intent("android.intent.action.MAIN");

      intent.setComponent(new ComponentName("com.connectingcustomer.mobile", "com.connectingcustomer.mobile.ConnectingCustomer"));
      intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

      Context context = this.cordova.getActivity().getApplicationContext();
      context.startActivity(intent);
    }
    else if(action.equals("setSpeakerPhone")) {
		
		boolean isEnabled = args.getBoolean(0);

		AudioManager m_amAudioManager;
        m_amAudioManager = (AudioManager)this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
        m_amAudioManager.setMode(AudioManager.MODE_IN_CALL);
        m_amAudioManager.setSpeakerphoneOn(false);
    }

    return true;

  };

};